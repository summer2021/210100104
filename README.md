项目成果保存于 Figma 中


### 获得所有文件的 edit access
https://www.figma.com/team_invite/redeem/bbdUlAfvDxnOqRd5jdCiPl


### 文件列表
1 理解用户 https://www.figma.com/file/3Wr5RVBGVEG0rmHAXphlnG/1-理解用户

2 定义问题 https://www.figma.com/file/Cx7aLDthnOtqW05AQGW98A/2-定义问题

3 构思想法 https://www.figma.com/file/R6lKP7Kwk7qWlAUVB8kBpj/3-构思想法

4 设计原型 https://www.figma.com/file/y9aSNZD0I3PTo6oko8Z3gH/4-设计原型

5 测试检验与交付 https://www.figma.com/file/cJpmD1cHgEiIDTsrRVmL7x/5-测试检验与交付
